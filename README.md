# LPD lean Demonstration Project based on Raspberry Pi.

## Components
    
+ Raspberry Pi 3 Model B
    
    https://www.raspberrypi.org/products/raspberry-pi-3-model-b/

+ Raspberry Pi Official 7" Touch Screen
    
    https://www.raspberrypi.org/products/raspberry-pi-touch-display/

+ Numato 32 Channel USB GPIO Module With Analog Inputs
    
    http://numato.com/32-channel-usb-gpio-module-with-analog-inputs/
    
+ Numato 16 Channel USB Relay Module
    
    http://numato.com/16-channel-usb-relay-module/
    
+ Proximity sensor
    
    https://www.amazon.com/gp/product/B00AMC1V2C/ref=oh_aui_search_detailpage?ie=UTF8&psc=1
    
+ DC motor: TBD

    https://www.servocity.com/40-rpm-gear-motor
    
    
## Assign unique ID to the Numato boards. (GPIO board is optional)

All Numato boards have ability to identify with their unique ID.

To identify multiple boards, we need to set their ID as given values.

Set ID of GPIO board as `22222222`, Relay as `11111111`.

Let us set ID of Numato boards one by one.

- Connect GPIO board only.

- Check its port name.
    
        ls /dev
    
    You can see something like this:
    ![ls /dev](img/lsdev.jpg "Result of 'ls /dev' command")
    
    As you can see, a Numato device is detected as `/dev/ttyACM0`.
    (Sometimes it will be detected as `/dev/ttyACM1` or `/dev/ttyACM2` or so)
    
- Assign device ID to it.
        
        cd ~/lpd_lean
        python set_id.py /dev/ttyACM0 11111111
    
    Port name is `/dev/ttyACM0`, and ID is `11111111` as described above.

- Remove GPIO board and connect relay board and perform same action.


## Pin Connection.

  ![RPi3 Pinout](img/raspberry_pi_2_pinout.jpg "Pinout of RPi 3")
    
  | **Raspberry Pi** |             **Peripheral Unit**          |
  | :----:           |   :----:                                 |
  |  GPIO4           |    Proximity Sensor of **PART COUNTER**  |
  |  Channel0 of USB Relay |    Motor of **PART COUNTER**           |
  |                  |                                          |
  |  GPIO17          |    Proximity Sensor1 of **POKA YOKE**    |
  |  GPIO27          |    Proximity Sensor2 of **POKA YOKE**    |
  |  GPIO22          |    Proximity Sensor3 of **POKA YOKE**    |
  |  GPIO23          |    Light output of **POKA YOKE**         |
  |                  |                                          |
  |  GPIO12          |   Proximity Sensor1 of **PICK to LIGHT** |
  |  GPIO16          |   Proximity Sensor2 of **PICK to LIGHT** |
  |  GPIO20          |   Proximity Sensor3 of **PICK to LIGHT** |
  |  GPIO21          |   Proximity Sensor4 of **PICK to LIGHT** |
  |  GPIO6           |    Light1 output of **PICK to LIGHT**    |
  |  GPIO13          |    Light2 output of **PICK to LIGHT**    |
  |  GPIO19          |    Light3 output of **PICK to LIGHT**    |
  |  GPIO26          |    Light4 output of **PICK to LIGHT**    |

## Install Kivy on RPi.
    
    sudo easy_install --upgrade pip
    
    sudo apt-get install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev \ 
    libsdl2-ttf-dev pkg-config libgl1-mesa-dev libgles2-mesa-dev \ 
    python-setuptools libgstreamer1.0-dev git-core \
    gstreamer1.0-plugins-{bad,base,good,ugly} \
    gstreamer1.0-{omx,alsa} python-dev
    
    sudo pip install Cython==0.23
    
    sudo pip install git+https://github.com/kivy/kivy.git@master
    
- Using Official RPi touch display (Optional)

    If you are using the official Raspberry Pi touch display, you need to configure Kivy to use it as an input source. 
    
    To do this, edit the file `~/.kivy/config.ini` and go to the [input] section. 
    Add this:

        mouse = mouse
        mtdev_%(name)s = probesysfs,provider=mtdev
        hid_%(name)s = probesysfs,provider=hidinput

- Show mouse cursor when using HDMI display
    
    Open the `~/.kivy/config.ini` and go to the [module] section, and insert follow:
        
        [modules]
        touchring=scale=.3,alpha=.7,show_cursor=1
        
**NOTE:** If we need to execute our app with `root` privileges, we just need to copy the config file to root user's config directory.
    
    sudo cp ~/.kivy/config.ini /root/.kivy/config.ini
    
    
- Run python Kivy app on HDMI display.
    
    If you cannot see Kivy app on your HDMI display, just add `KIVY_BCM_DISPMANX_ID=2` before executing python script.
    
        KIVY_BCM_DISPMANX_ID=2 python main.py


## Barcode scanner

### Install dependencies

    sudo apt-get install evince
   
### Create desktop shortcut for barcode reader
    
    nano ~/Desktop/barcode_reader.desktop
    
And add this:
    
    [Desktop Entry]
    Name=Barcode Scanner
    Comment=Barcode Scanner
    Icon=/home/pi/lpd_lean/icon.img
    Exec=sudo /usr/bin/python /home/pi/lpd_lean/barcode/barcode_reader.py
    Type=Application
    Encoding=UTF-8
    Terminal=true
    Categories=None;
    
## Part Counter
    
### Create desktop shortcut for Part Counter
    
    nano ~/Desktop/part_counter.desktop
    
And add this:
    
    [Desktop Entry]
    Name=Part Counter
    Comment=Part Counter
    Icon=/home/pi/lpd_lean/icon.img
    Exec=sudo /usr/bin/python /home/pi/lpd_lean/part_counter/main.py
    Type=Application
    Encoding=UTF-8
    Terminal=true
    Categories=None;
    
### Create desktop shortcut for Pick to Light
    
    nano ~/Desktop/pick_light.desktop
    
And add this:
    
    [Desktop Entry]
    Name=Pick to Light
    Comment=Pick to Light
    Icon=/home/pi/lpd_lean/icon.img
    Exec=sudo /usr/bin/python /home/pi/lpd_lean/pick_light/main.py
    Type=Application
    Encoding=UTF-8
    Terminal=true
    Categories=None;

### Create desktop shortcut for Poke Yoke Fixture
    
    nano ~/Desktop/poka_yoke.desktop
    
And add this:
    
    [Desktop Entry]
    Name=Poka Yoke
    Comment=Poka Yoke
    Icon=/home/pi/lpd_lean/icon.img
    Exec=sudo /usr/bin/python /home/pi/lpd_lean/poka_yoke/main.py
    Type=Application
    Encoding=UTF-8
    Terminal=true
    Categories=None;