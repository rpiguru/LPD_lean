import os
import platform
import threading

import time

b_rpi = True if platform.system() == 'Linux' else False

if b_rpi:
    import RPi.GPIO as GPIO


class PickLight:

    sensor_pins = [12, 16, 20, 21]
    led_pins = [6, 13, 19, 26]

    lock = threading.RLock()
    cur_num = 0

    def __init__(self):
        if b_rpi:
            GPIO.setmode(GPIO.BCM)
            for i in range(len(self.sensor_pins)):
                GPIO.setup(self.sensor_pins[i], GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
                GPIO.setup(self.led_pins[i], GPIO.OUT, initial=0)
            GPIO.output(self.led_pins[0], 1)
            self.cur_num = 0

    def start(self):

        GPIO.add_event_detect(self.sensor_pins[0], GPIO.RISING, callback=self.pin_event_0, bouncetime=100)
        GPIO.add_event_detect(self.sensor_pins[1], GPIO.RISING, callback=self.pin_event_1, bouncetime=100)
        GPIO.add_event_detect(self.sensor_pins[2], GPIO.RISING, callback=self.pin_event_2, bouncetime=100)
        GPIO.add_event_detect(self.sensor_pins[3], GPIO.RISING, callback=self.pin_event_3, bouncetime=100)

        print('Reading sensor values, press Ctrl-C to quit...')
        while True:
            time.sleep(1)

    def pin_event_0(self, *args):
        with self.lock:
            if self.cur_num == 0:
                print 'Detected sensor number: 0, turning LED {} on'.format(self.led_pins[1])
                GPIO.output(self.led_pins[0], 0)
                GPIO.output(self.led_pins[1], 1)
                self.cur_num = 1

    def pin_event_1(self, *args):
        with self.lock:
            if self.cur_num == 1:
                print 'Detected sensor number: 1, turning LED {} on'.format(self.led_pins[2])
                GPIO.output(self.led_pins[1], 0)
                GPIO.output(self.led_pins[2], 1)
                self.cur_num = 2

    def pin_event_2(self, *args):
        with self.lock:
            if self.cur_num == 2:
                print 'Detected sensor number: 2, turning LED {} on'.format(self.led_pins[3])
                GPIO.output(self.led_pins[2], 0)
                GPIO.output(self.led_pins[3], 1)
                self.cur_num = 3

    def pin_event_3(self, *args):
        with self.lock:
            if self.cur_num == 3:
                print 'Detected sensor number: 3, turning LED {} on'.format(self.led_pins[0])
                GPIO.output(self.led_pins[3], 0)
                GPIO.output(self.led_pins[0], 1)
                self.cur_num = 0

    @staticmethod
    def stop():
        GPIO.cleanup()


if __name__ == '__main__':

    if b_rpi:
        if os.geteuid() != 0:
            exit("You need to have root privileges to run this script.\n"
                 "Please try again, this time using 'sudo'. Exiting.")
    a = PickLight()

    a.start()
