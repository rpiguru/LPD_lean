import os
import platform
import threading
import time

b_rpi = True if platform.system() == 'Linux' else False

if b_rpi:
    import RPi.GPIO as GPIO


class PokaYoke:

    led_pin = 23
    sensor_pins = [17, 27, 22]
    sensor_state = [False] * 3

    lock = threading.RLock()

    def __init__(self, led_pin=23):
        self.led_pin = led_pin
        if b_rpi:
            GPIO.setwarnings(False)
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(self.led_pin, GPIO.OUT, initial=0)
            for i in range(len(self.sensor_pins)):
                GPIO.setup(self.sensor_pins[i], GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def start(self):
        # GPIO.add_event_detect(self.sensor_pins[0], GPIO.BOTH, callback=self.pin_event_0, bouncetime=100)
        # GPIO.add_event_detect(self.sensor_pins[1], GPIO.BOTH, callback=self.pin_event_1, bouncetime=100)
        # GPIO.add_event_detect(self.sensor_pins[2], GPIO.BOTH, callback=self.pin_event_2, bouncetime=100)
        print('Reading sensor values, press Ctrl-C to quit...')
        while True:
            state = [GPIO.input(self.sensor_pins[0]), GPIO.input(self.sensor_pins[1]), GPIO.input(self.sensor_pins[2])]
            if not GPIO.input(self.sensor_pins[0]) and not GPIO.input(self.sensor_pins[1]) and not GPIO.input(
                    self.sensor_pins[2]):
                GPIO.output(self.led_pin, 1)
                print 'All parts are detected.'
            else:
                GPIO.output(self.led_pin, 0)
            print state
            time.sleep(.1)

    # def pin_event_0(self, *args):
    #     with self.lock:
    #         if GPIO.input(self.sensor_pins[0]):
    #             self.sensor_state[0] = True
    #         else:
    #             self.sensor_state[0] = False
    #         self.check_all()
    #
    # def pin_event_1(self, *args):
    #     with self.lock:
    #         if GPIO.input(self.sensor_pins[1]):
    #             self.sensor_state[1] = True
    #         else:
    #             self.sensor_state[1] = False
    #         self.check_all()
    #
    # def pin_event_2(self, *args):
    #     with self.lock:
    #         if GPIO.input(self.sensor_pins[2]):
    #             self.sensor_state[2] = True
    #         else:
    #             self.sensor_state[2] = False
    #         self.check_all()
    #
    # def check_all(self):
    #     print 'Current state: ', self.sensor_state
    #     if self.sensor_state == [True] * 3:
    #         GPIO.output(self.led_pin, 1)
    #         print 'All parts are detected.'
    #     else:
    #         GPIO.output(self.led_pin, 0)
    #         print 'One or more parts are not detected.'

    @staticmethod
    def stop():
        GPIO.cleanup()


if __name__ == '__main__':

    if b_rpi:
        if os.geteuid() != 0:
            exit("You need to have root privileges to run this script.\n"
                 "Please try again, this time using 'sudo'. Exiting.")
    a = PokaYoke()

    a.start()
