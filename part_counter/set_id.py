"""
Python script to set ID of the Numato device.

Usage: python set_id.py /dev/ttyACM0 11111111

"""

import time
import serial
import sys


class NumatoCtrl:

    def __init__(self, serial_port):
        self.ser = serial.Serial(port=serial_port, baudrate=9600, timeout=0.3)

    def get_id(self):
        self.write_cmd('id get')
        return self.read_data()

    def set_id(self, new_id):
        return self.write_cmd('id set ' + str(new_id))

    def write_cmd(self, cmd):
        """
        Send command to the serial by adding CR at the end of it.
        Since pyserial's write command outputs the given string back, it is needed to read back. (See code below)
        """
        cmd_buf = cmd + "\r"
        self.ser.write(cmd_buf)
        # since 'write' command outputs the given string over the serial port, we need read back...
        if self.read_data() != cmd:
            return False
        else:
            return True

    def read_data(self):
        line = ""
        s_time = time.time()
        while True:
            if time.time() - s_time >= 0.5:
                break
            try:
                data = self.ser.read()
                if data == "\r":
                    break
                else:
                    line = line + data
            except serial.SerialException as e:
                print "Device is too busy:   ", e
                time.sleep(0.05)

        return line.strip()

    def get_version(self):
        self.write_cmd("ver")
        return self.read_data()


if __name__ == '__main__':

    if len(sys.argv) < 3:
        print "Please input port name and its new ID"
        print "USAGE: python set_id.py /dev/ttyACM0 12345678"
        exit(0)

    port = sys.argv[1]
    new_id = sys.argv[2]

    try:
        ctrl = NumatoCtrl(port)
        ctrl.set_id(new_id)
        if new_id == ctrl.get_id():
            print "Succeeded to set new ID, new ID is ", new_id
        else:
            print "Failed to set new ID, please try again later."

    except serial.SerialException as e:
        print e



