"""
GPIO controller Class for Numato's 32 Channel USB GPIO board.
    http://numato.com/32-channel-usb-gpio-module-with-analog-inputs/

For technical support:
    https://docs.numato.com/doc/32-channel-usb-gpio-module-with-analog-inputs/

"""
import time

import serial


class GPIOCtrl:

    relay_cnt = 0

    b_read = False

    def __init__(self, serial_port):
        self.ser = serial.Serial(port=serial_port, baudrate=9600, timeout=0.3)
        self.b_read = False
        t = self.read_all()
        if t is not None:
            self.relay_cnt = len(t)
        else:  # try again
            t = self.read_all()
            if t is not None:
                self.relay_cnt = len(t)

    def get_id(self):
        self.write_cmd('id get')
        return self.read_data()

    def set_id(self, new_id):
        return self.write_cmd('id set ' + str(new_id))

    def write_cmd(self, cmd):
        """
        Send command to the serial by adding CR at the end of it.
        Since pyserial's write command outputs the given string back, it is needed to read back. (See code below)
        """
        cmd_buf = cmd + "\r"
        while self.b_read:
            time.sleep(0.1)
        self.ser.write(cmd_buf)
        # since 'write' command outputs the given string over the serial port, we need read back...
        if self.read_data() != cmd:
            return False
        else:
            return True

    def read_data(self):
        self.b_read = True
        line = ""
        s_time = time.time()
        while True:
            if time.time() - s_time >= 0.5:
                break
            try:
                data = self.ser.read()
                if data == "\r":
                    break
                else:
                    line = line + data
            except serial.SerialException as e:
                print "Device is too busy:   ", e
                time.sleep(0.05)
            except OSError as e:
                print "Device is temporarily unavailable:   ", e
                time.sleep(0.05)

        self.b_read = False
        return line.strip()

    def get_version(self):
        self.write_cmd("ver")
        return self.read_data()

    def initialize(self):
        self.unmask_all()
        self.set_gpio_dir()

    def unmask_all(self):
        """
        Unmask all GPIO to enable them since we are using all GPIO...
        """
        if self.relay_cnt == 32:
            return self.write_cmd('gpio iomask ffffffff')
        elif self.relay_cnt == 16:
            return self.write_cmd('gpio iomask ffff')

    def set_gpio_dir(self):
        """
        Set IO direction of all pins
        """
        if self.relay_cnt == 32:
            return self.write_cmd('gpio iodir ffffffff')  # Sets all GPIO to input
        elif self.relay_cnt == 16:
            return self.write_cmd('gpio iodir ffff')  # Sets all GPIO to input

    def read_all(self):
        self.write_cmd("gpio readall")
        hex_val = self.read_data()
        try:
            bin_val = bin(int(hex_val, 16))[2:].zfill(len(hex_val) * 4)
            return bin_val[::-1]
        except ValueError as e:
            print "Error, ", e
            print "VAL:", hex_val
            return None

if __name__ == '__main__':

    s_time = time.time()

    ctrl = GPIOCtrl('/dev/ttyACM0')
    ctrl.initialize()

    # print ctrl.set_id(11111111)

    print "ID: ", ctrl.get_id()
    print "Version: ", ctrl.get_version()

    while True:
        input_val = raw_input("Enter command: ")
        tmp = input_val.split(",")
        cmd = tmp[0]
        s_time = time.time()
        if cmd == 'readall':
            print ctrl.read_all()
        elif cmd == 'poll':
            try:
                while True:
                    s_time = time.time()
                    print "Read All: ", ctrl.read_all()
                    if time.time() - s_time < float(tmp[1]):
                        time.sleep(float(tmp[1]) + s_time - time.time())

            except KeyboardInterrupt:  # catches the ctrl-c command, which breaks the loop above
                print("Continuous polling stopped")

        print "Elapsed: ", time.time() - s_time

