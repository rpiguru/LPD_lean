# -*- coding: utf-8 -*-

import glob
import os
import platform
import sys
import datetime
import threading
import time
from kivy import Config
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty, ObjectProperty, NumericProperty, BooleanProperty
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import SlideTransition
from serial import SerialException
import config
from gpio_ctrl import GPIOCtrl
from relay_ctrl import RelayCtrl

b_rpi = True if platform.system() == 'Linux' else False

if b_rpi:
    import RPi.GPIO as GPIO

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'


debounce_time = 300          # De-bounce time for proximity sensor in milli-sec


class CautionPopup(Popup):
    pass


class ConfirmPopup(Popup):
    pass


class MainApp(App):
    current_title = StringProperty()        # Store title of current screen
    screen_names = ListProperty([])
    screens = {}                             # Dict of all screens
    hierarchy = ListProperty([])

    # Popup instances
    caution_popup = ObjectProperty(None)
    confirm_popup = ObjectProperty(None)

    gpio_ctrl = ObjectProperty(None)
    relay_ctrl = ObjectProperty(None)

    count = NumericProperty(0)

    b_stop = BooleanProperty(True)
    lock = ObjectProperty(None)

    last_time = NumericProperty(None)

    def build(self):
        """
        base function of kivy app
        :return:
        """
        self.load_screen()

        self.initialize()

        self.caution_popup = CautionPopup()
        self.confirm_popup = ConfirmPopup()

        self.b_stop = True
        self.lock = threading.RLock()

        self.go_screen('entry', 'right')

        self.last_time = time.time()

        if not self.assign_numato_boards():
            self.caution_popup.ids['lb_content'].text = 'Error, failed to detect Relay/GPIO board.'
            self.caution_popup.open()

    def initialize(self):
        """
        Initialize
        :return:
        """
        if b_rpi:
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(config.proximity_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
            GPIO.add_event_detect(config.proximity_pin, GPIO.RISING, callback=self.count_part, bouncetime=debounce_time)

    def assign_numato_boards(self):
        """
        Assign the Numato boards via their ID
        RELAY BOARD   : 11111111
        GPIO BOARD    : 22222222
        """
        for addr in glob.glob('/dev/ttyACM*'):
            try:
                print " --- Trying to identify ", addr
                tmp_dev = GPIOCtrl(addr)
                tmp_id = tmp_dev.get_id()
                if tmp_id == str('22222222'):
                    self.gpio_ctrl = GPIOCtrl(addr)
                    print datetime.datetime.now(), ": ", "GPIO: ", self.gpio_ctrl.get_id(), ",  ", addr

                elif tmp_id == str('11111111'):
                    self.relay_ctrl = RelayCtrl(addr)
                    print datetime.datetime.now(), ": ", "Relay 1: ", self.relay_ctrl1.get_id(), ",  ", addr
                    self.relay_ctrl.turn_off_all()

                else:
                    print datetime.datetime.now(), ": ", 'Error, unknown device found.'
                    return False

            except SerialException as e:
                print datetime.datetime.now(), ": ", 'Error, ', e
            except OSError as e:
                print datetime.datetime.now(), ": ", 'Error, ', e
                return False

        # assign manually
        # self.gpio_ctrl = GPIOCtrl('/dev/ttyACM1')
        # self.relay_ctrl1 = RelayCtrl('/dev/ttyACM0')
        # self.relay_ctrl1.turn_off_all()
        # self.relay_ctrl2 = RelayCtrl('/dev/ttyACM2')
        # self.relay_ctrl2.turn_off_all()

        return True

    def confirm_yes(self):
        """
        callback function of "Yes" button in confirm popup
        :return:
        """
        self.confirm_popup.dismiss()

    def confirm_no(self):
        """
        callback function of "No" button in confirm popup
        :return:
        """
        self.confirm_popup.dismiss()

    def go_screen(self, dest_screen, direction):
        """
        Go to given screen
        :param dest_screen:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        sm = self.root.ids.sm

        sm.transition = SlideTransition()
        screen = self.screens[dest_screen]
        sm.switch_to(screen, direction=direction)
        self.current_title = screen.name

    def load_screen(self):
        """
        Load all screens from data/screens to Screen Manager
        :return:
        """
        available_screens = []

        full_path_screens = glob.glob(cur_dir + "screens/*.kv")

        for file_path in full_path_screens:
            file_name = os.path.basename(file_path)
            available_screens.append(file_name.split(".")[0])

        self.screen_names = available_screens
        for i in range(len(full_path_screens)):
            screen = Builder.load_file(full_path_screens[i])
            self.screens[available_screens[i]] = screen
        return True

    def btn_start(self, btn):
        if btn.text == 'PAUSE':
            btn.text = 'RESUME'
            if self.relay_ctrl is not None:
                self.relay_ctrl.set_relay(config.motor_num, False)
            self.b_stop = True
        else:
            btn.text = 'PAUSE'
            self.screens['entry'].ids['lb_cnt'].text = str(self.count)
            if self.relay_ctrl is not None:
                self.relay_ctrl.set_relay(config.motor_num, True)
            self.b_stop = False

    def btn_reset(self):
        self.count = 0
        self.screens['entry'].ids['lb_cnt'].text = str(self.count)

    def count_part(self, *args):
        if not self.b_stop:
            if time.time() - self.last_time > debounce_time / 1000:
                self.count += 1
                self.screens['entry'].ids['lb_cnt'].text = str(self.count)
                self.last_time = time.time()
                print 'Part is counted, ', self.count


if __name__ == '__main__':

    if b_rpi:
        if os.geteuid() != 0:
            exit("You need to have root privileges to run this script.\n"
                 "Please try again, this time using 'sudo'. Exiting.")

    # if Config.getint('graphics', 'width') != 800:
    #     Config.set('graphics', 'width', '800')
    #     Config.write()
    #     print 'Sorry, window width is not set correctly, please restart app now.'
    #     sys.exit()
    # elif Config.getint('graphics', 'height') != 480:
    #     Config.set('graphics', 'height', '480')
    #     Config.write()
    #     print 'Sorry, window height is not set correctly, please restart app now.'
    #     sys.exit()

    app = MainApp()
    app.run()
