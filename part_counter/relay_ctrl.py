"""
Relay controller class for Numato Multi-channel USB Relay Boards.

Product page: http://numato.com/32-channel-usb-relay-module/

"""
import threading
import time

import datetime
import serial


# callback function for "retry" decorator
# def retry(func):
#     def retried_func(*args*kwargs):
#         MAX_TRIES = 3
#         tries = 0
#         resp = False
#         while True:
#             resp = func(*args, **kwargs)
#             if not resp and tries < MAX_TRIES:
#                 tries += 1
#                 continue
#             break
#         return resp
#     return retried_func()


class RelayCtrl:
    ser = None
    cmd_list = '0123456789ABCDEFGHIJKLMNOPQRSTUV'
    port = None
    relay_cnt = 0

    b_reading = False

    def __init__(self, serial_port):
        self.port = serial_port
        self.b_reading = False
        self.ser = serial.Serial(port=serial_port, baudrate=9600, timeout=0.5)
        n = self.read_all()
        if n is not None:
            self.relay_cnt = len(n)

        print "Number of relays: ", self.relay_cnt

    def get_id(self):
        self.write_cmd('id get')
        return self.read_data()

    def set_id(self, new_id):
        return self.write_cmd('id set ' + str(new_id))

    def write_cmd(self, cmd):
        """
        Send command to the serial by adding CR at the end of it.
        Since pyserial's write command outputs the given string back, it is needed to read back. (See code below)
        """
        cmd_buf = cmd + "\r"
        while self.b_reading:
            print datetime.datetime.now(), ": Waiting for finishing previous reading from relay... cmd: ", cmd, \
                ", port: ", self.port
            time.sleep(0.01)
        self.ser.write(cmd_buf)
        # since 'write' command outputs the given string over the serial port, we need read back...
        resp = self.read_data()
        if resp[1:] not in cmd:  # response must be "<cmd"
            print datetime.datetime.now(), ':  Error of relay, cmd: ', cmd, ', response: ', resp, " , port: ", self.port
            print 'Retrying to read...'
            val = self.read_data()
            if val[1:] not in cmd:
                print 'Really strange value...!!!, expected: ', cmd, ' val: ', val
                return True
            else:
                print 'Recovered...'
                return False
        else:
            return True

    def read_data(self):
        self.b_reading = True
        line = ""
        s_time = time.time()
        while True:
            if time.time() - s_time >= 0.5:
                break
            try:
                data = self.ser.read()
                if data == "\r":
                    break
                else:
                    line = line + data
            except serial.SerialException as e:
                print "Device is too busy:   ", e
                time.sleep(0.01)
            except OSError as e:
                print "OS Error,   ", e
                time.sleep(0.01)

        self.b_reading = False
        return line.strip()

    def get_version(self):
        self.write_cmd("ver")
        return self.read_data()

    def set_relay(self, num, val):
        """
        Set relay as ON/OFF
        :param num: relay number
        :param val: 'on' or 'off'
        :return:
        """
        if val not in ['on', 'off']:
            return False
        else:
            new_cmd = "relay " + val + " " + self.cmd_list[num]
            t = threading.Thread(target=self.execute_thread, args=(new_cmd,))
            t.daemon = True
            t.start()
            # return self.write_cmd(new_cmd)

    def execute_thread(self, c):
        self.write_cmd(c)

    def get_relay(self, num):
        """
        Get status of relay
        :param num: number of relay
        :return:
        """
        self.write_cmd("relay read " + self.cmd_list[num])
        return self.read_data()

    def reset_relay(self):
        return self.write_cmd("reset")

    def read_all(self):
        new_cmd = "relay readall"
        tt = threading.Thread(target=self.execute_thread, args=(new_cmd,))
        tt.daemon = True
        tt.start()
        tt.join()
        hex_val = self.read_data()
        # print "Read data: ", hex_val
        try:
            bin_val = bin(int(hex_val, 16))[2:].zfill(len(hex_val) * 4)
            return bin_val[::-1]
        except ValueError:
            print "Unexpected Value: ", hex_val.strip()
            if hex_val.strip() == ">relay read_all":  # try again
                try:
                    bin_val = bin(int(hex_val, 16))[2:].zfill(len(hex_val) * 4)
                    return bin_val[::-1]
                except ValueError:
                    print "Strange value received (tried twice), val: ", hex_val
                    return None
            else:
                print "Strange value received, val: ", hex_val
                return None

    # @retry
    def read_all_with_decorator(self):
        new_cmd = "relay readall"
        tt = threading.Thread(target=self.execute_thread, args=(new_cmd,))
        tt.daemon = True
        tt.start()
        tt.join()
        hex_val = self.read_data()
        # print "Read data: ", hex_val
        try:
            bin_val = bin(int(hex_val, 16))[2:].zfill(len(hex_val) * 4)
            return bin_val[::-1]
        except ValueError:
            print "Unexpected Value: ", hex_val.strip()
            return False

    def turn_off_all(self):
        if self.relay_cnt == 32:
            self.write_cmd("relay writeall 00000000")
        elif self.relay_cnt == 16:
            self.write_cmd("relay writeall 0000")
        elif self.relay_cnt == 8:
            self.write_cmd("relay writeall 00")
        else:   # default
            self.write_cmd("relay writeall 00000000")

if __name__ == '__main__':

    s_time = time.time()

    ctrl = RelayCtrl('/dev/ttyACM2')

    # print ctrl.set_id(11111111)

    print "ID: ", ctrl.get_id()
    print "Version: ", ctrl.get_version()

    while True:
        input_val = raw_input("Enter command: ")
        tmp = input_val.split(",")
        cmd = tmp[0]
        s_time = time.time()
        if cmd == 'set':
            param = tmp[1]
            print "Setting relay ", param, " as ", tmp[2]
            ctrl.set_relay(int(param), tmp[2])
        elif cmd == 'get':
            param = tmp[1]
            print "Getting status of relay ", param
            print ctrl.get_relay(int(param))
        elif cmd == 'readall':
            print ctrl.read_all()
        elif cmd == 'poll':
            try:
                while True:
                    s_time = time.time()
                    print "Read All: ", ctrl.read_all()
                    if time.time() - s_time < float(tmp[1]):
                        time.sleep(float(tmp[1]) + s_time - time.time())

            except KeyboardInterrupt:  # catches the ctrl-c command, which breaks the loop above
                print("Continuous polling stopped")

        print "Elapsed: ", time.time() - s_time

